var _WM_APP_PROPERTIES = {
  "activeTheme" : "flyer",
  "dateFormat" : "",
  "defaultLanguage" : "en",
  "displayName" : "Planning Manager",
  "homePage" : "Main",
  "name" : "Planning_Manager",
  "platformType" : "WEB",
  "supportedLanguages" : "en",
  "timeFormat" : "",
  "type" : "APPLICATION",
  "version" : "1.0"
};