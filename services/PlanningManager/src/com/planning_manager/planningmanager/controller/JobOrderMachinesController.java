/*Copyright (c) 2017-2018 vanenburgsoftware.com All Rights Reserved.
 This software is the confidential and proprietary information of vanenburgsoftware.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with vanenburgsoftware.com*/
package com.planning_manager.planningmanager.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.planning_manager.planningmanager.JobOrderMachines;
import com.planning_manager.planningmanager.service.JobOrderMachinesService;


/**
 * Controller object for domain model class JobOrderMachines.
 * @see JobOrderMachines
 */
@RestController("PlanningManager.JobOrderMachinesController")
@Api(value = "JobOrderMachinesController", description = "Exposes APIs to work with JobOrderMachines resource.")
@RequestMapping("/PlanningManager/JobOrderMachines")
public class JobOrderMachinesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobOrderMachinesController.class);

    @Autowired
	@Qualifier("PlanningManager.JobOrderMachinesService")
	private JobOrderMachinesService jobOrderMachinesService;

	@ApiOperation(value = "Creates a new JobOrderMachines instance.")
	@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public JobOrderMachines createJobOrderMachines(@RequestBody JobOrderMachines jobOrderMachines) {
		LOGGER.debug("Create JobOrderMachines with information: {}" , jobOrderMachines);

		jobOrderMachines = jobOrderMachinesService.create(jobOrderMachines);
		LOGGER.debug("Created JobOrderMachines with information: {}" , jobOrderMachines);

	    return jobOrderMachines;
	}


    @ApiOperation(value = "Returns the JobOrderMachines instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public JobOrderMachines getJobOrderMachines(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting JobOrderMachines with id: {}" , id);

        JobOrderMachines foundJobOrderMachines = jobOrderMachinesService.getById(id);
        LOGGER.debug("JobOrderMachines details with id: {}" , foundJobOrderMachines);

        return foundJobOrderMachines;
    }

    @ApiOperation(value = "Updates the JobOrderMachines instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public JobOrderMachines editJobOrderMachines(@PathVariable("id") Integer id, @RequestBody JobOrderMachines jobOrderMachines) throws EntityNotFoundException {
        LOGGER.debug("Editing JobOrderMachines with id: {}" , jobOrderMachines.getId());

        jobOrderMachines.setId(id);
        jobOrderMachines = jobOrderMachinesService.update(jobOrderMachines);
        LOGGER.debug("JobOrderMachines details with id: {}" , jobOrderMachines);

        return jobOrderMachines;
    }

    @ApiOperation(value = "Deletes the JobOrderMachines instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteJobOrderMachines(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting JobOrderMachines with id: {}" , id);

        JobOrderMachines deletedJobOrderMachines = jobOrderMachinesService.delete(id);

        return deletedJobOrderMachines != null;
    }

    /**
     * @deprecated Use {@link #findJobOrderMachines(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of JobOrderMachines instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<JobOrderMachines> searchJobOrderMachinesByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering JobOrderMachines list");
        return jobOrderMachinesService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of JobOrderMachines instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<JobOrderMachines> findJobOrderMachines(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering JobOrderMachines list");
        return jobOrderMachinesService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of JobOrderMachines instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<JobOrderMachines> filterJobOrderMachines(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering JobOrderMachines list");
        return jobOrderMachinesService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportJobOrderMachines(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return jobOrderMachinesService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of JobOrderMachines instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countJobOrderMachines( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting JobOrderMachines");
		return jobOrderMachinesService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getJobOrderMachinesAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return jobOrderMachinesService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service JobOrderMachinesService instance
	 */
	protected void setJobOrderMachinesService(JobOrderMachinesService service) {
		this.jobOrderMachinesService = service;
	}

}

