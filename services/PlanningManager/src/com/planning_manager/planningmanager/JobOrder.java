/*Copyright (c) 2017-2018 vanenburgsoftware.com All Rights Reserved.
 This software is the confidential and proprietary information of vanenburgsoftware.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with vanenburgsoftware.com*/
package com.planning_manager.planningmanager;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * JobOrder generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`Job Order`")
public class JobOrder implements Serializable {

    private Integer id;
    private Integer prodOrdId;
    private Integer jobNo;
    private String name;
    private Integer depId;
    private String jobStat;
    private Integer clustId;
    private Integer jobTpId;
    private Date plStart;
    private Date plEnd;
    @Type(type = "DateTime")
    private LocalDateTime execStart;
    @Type(type = "DateTime")
    private LocalDateTime execEnd;
    private Integer prioId;
    private String ordrCode;
    private JobType jobType;
    private ProductionOrder productionOrder;
    private Department department;
    private Priorities priorities;
    private Cluster cluster;
    private List<JobOrderEquipment> jobOrderEquipments;
    private List<JobOrderOperators> jobOrderOperatorses;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ID`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`PROD_ORD_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getProdOrdId() {
        return this.prodOrdId;
    }

    public void setProdOrdId(Integer prodOrdId) {
        this.prodOrdId = prodOrdId;
    }

    @Column(name = "`JOB_NO`", nullable = true, scale = 0, precision = 10)
    public Integer getJobNo() {
        return this.jobNo;
    }

    public void setJobNo(Integer jobNo) {
        this.jobNo = jobNo;
    }

    @Column(name = "`NAME`", nullable = true, length = 255)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "`DEP_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getDepId() {
        return this.depId;
    }

    public void setDepId(Integer depId) {
        this.depId = depId;
    }

    @Column(name = "`JOB_STAT`", nullable = true, length = 255)
    public String getJobStat() {
        return this.jobStat;
    }

    public void setJobStat(String jobStat) {
        this.jobStat = jobStat;
    }

    @Column(name = "`CLUST_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getClustId() {
        return this.clustId;
    }

    public void setClustId(Integer clustId) {
        this.clustId = clustId;
    }

    @Column(name = "`JOB_TP_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getJobTpId() {
        return this.jobTpId;
    }

    public void setJobTpId(Integer jobTpId) {
        this.jobTpId = jobTpId;
    }

    @Column(name = "`PL_START`", nullable = true)
    public Date getPlStart() {
        return this.plStart;
    }

    public void setPlStart(Date plStart) {
        this.plStart = plStart;
    }

    @Column(name = "`PL_END`", nullable = true)
    public Date getPlEnd() {
        return this.plEnd;
    }

    public void setPlEnd(Date plEnd) {
        this.plEnd = plEnd;
    }

    @Column(name = "`EXEC_START`", nullable = true)
    public LocalDateTime getExecStart() {
        return this.execStart;
    }

    public void setExecStart(LocalDateTime execStart) {
        this.execStart = execStart;
    }

    @Column(name = "`EXEC_END`", nullable = true)
    public LocalDateTime getExecEnd() {
        return this.execEnd;
    }

    public void setExecEnd(LocalDateTime execEnd) {
        this.execEnd = execEnd;
    }

    @Column(name = "`PRIO_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getPrioId() {
        return this.prioId;
    }

    public void setPrioId(Integer prioId) {
        this.prioId = prioId;
    }

    @Column(name = "`ORDR_CODE`", nullable = true, length = 10)
    public String getOrdrCode() {
        return this.ordrCode;
    }

    public void setOrdrCode(String ordrCode) {
        this.ordrCode = ordrCode;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`JOB_TP_ID`", referencedColumnName = "`ID`", insertable = false, updatable = false)
    public JobType getJobType() {
        return this.jobType;
    }

    public void setJobType(JobType jobType) {
        if(jobType != null) {
            this.jobTpId = jobType.getId();
        }

        this.jobType = jobType;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`PROD_ORD_ID`", referencedColumnName = "`ID`", insertable = false, updatable = false)
    public ProductionOrder getProductionOrder() {
        return this.productionOrder;
    }

    public void setProductionOrder(ProductionOrder productionOrder) {
        if(productionOrder != null) {
            this.prodOrdId = productionOrder.getId();
        }

        this.productionOrder = productionOrder;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`DEP_ID`", referencedColumnName = "`ID`", insertable = false, updatable = false)
    public Department getDepartment() {
        return this.department;
    }

    public void setDepartment(Department department) {
        if(department != null) {
            this.depId = department.getId();
        }

        this.department = department;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`PRIO_ID`", referencedColumnName = "`ID`", insertable = false, updatable = false)
    public Priorities getPriorities() {
        return this.priorities;
    }

    public void setPriorities(Priorities priorities) {
        if(priorities != null) {
            this.prioId = priorities.getId();
        }

        this.priorities = priorities;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`CLUST_ID`", referencedColumnName = "`ID`", insertable = false, updatable = false)
    public Cluster getCluster() {
        return this.cluster;
    }

    public void setCluster(Cluster cluster) {
        if(cluster != null) {
            this.clustId = cluster.getId();
        }

        this.cluster = cluster;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "jobOrder")
    public List<JobOrderEquipment> getJobOrderEquipments() {
        return this.jobOrderEquipments;
    }

    public void setJobOrderEquipments(List<JobOrderEquipment> jobOrderEquipments) {
        this.jobOrderEquipments = jobOrderEquipments;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "jobOrder")
    public List<JobOrderOperators> getJobOrderOperatorses() {
        return this.jobOrderOperatorses;
    }

    public void setJobOrderOperatorses(List<JobOrderOperators> jobOrderOperatorses) {
        this.jobOrderOperatorses = jobOrderOperatorses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JobOrder)) return false;
        final JobOrder jobOrder = (JobOrder) o;
        return Objects.equals(getId(), jobOrder.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

