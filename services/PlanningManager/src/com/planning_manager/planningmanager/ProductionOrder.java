/*Copyright (c) 2017-2018 vanenburgsoftware.com All Rights Reserved.
 This software is the confidential and proprietary information of vanenburgsoftware.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with vanenburgsoftware.com*/
package com.planning_manager.planningmanager;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * ProductionOrder generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`Production Order`")
public class ProductionOrder implements Serializable {

    private Integer id;
    private Integer progrId;
    private Double ordrQuan;
    private Integer plannerId;
    @Type(type = "DateTime")
    private LocalDateTime plStart;
    private Date plStartMrp;
    private String ordrStat;
    private String serial;
    private Integer prioId;
    private String ordrCode;
    @Type(type = "DateTime")
    private LocalDateTime execStart;
    @Type(type = "DateTime")
    private LocalDateTime execEnd;
    private String ncNumb;
    @Type(type = "DateTime")
    private LocalDateTime plEnd;
    private Integer prodId;
    private Employee employee;
    private Program program;
    private Product product;
    private Priorities priorities;
    private List<JobOrder> jobOrders;
    private List<JobOrderMachines> jobOrderMachineses;
    private List<ScheduleLines> scheduleLineses;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ID`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`PROGR_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getProgrId() {
        return this.progrId;
    }

    public void setProgrId(Integer progrId) {
        this.progrId = progrId;
    }

    @Column(name = "`ORDR_QUAN`", nullable = true, scale = 0, precision = 10)
    public Double getOrdrQuan() {
        return this.ordrQuan;
    }

    public void setOrdrQuan(Double ordrQuan) {
        this.ordrQuan = ordrQuan;
    }

    @Column(name = "`PLANNER_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getPlannerId() {
        return this.plannerId;
    }

    public void setPlannerId(Integer plannerId) {
        this.plannerId = plannerId;
    }

    @Column(name = "`PL_START`", nullable = true)
    public LocalDateTime getPlStart() {
        return this.plStart;
    }

    public void setPlStart(LocalDateTime plStart) {
        this.plStart = plStart;
    }

    @Column(name = "`PL_START_MRP`", nullable = true)
    public Date getPlStartMrp() {
        return this.plStartMrp;
    }

    public void setPlStartMrp(Date plStartMrp) {
        this.plStartMrp = plStartMrp;
    }

    @Column(name = "`ORDR_STAT`", nullable = true, length = 255)
    public String getOrdrStat() {
        return this.ordrStat;
    }

    public void setOrdrStat(String ordrStat) {
        this.ordrStat = ordrStat;
    }

    @Column(name = "`SERIAL`", nullable = true, length = 255)
    public String getSerial() {
        return this.serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    @Column(name = "`PRIO_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getPrioId() {
        return this.prioId;
    }

    public void setPrioId(Integer prioId) {
        this.prioId = prioId;
    }

    @Column(name = "`ORDR_CODE`", nullable = true, length = 10)
    public String getOrdrCode() {
        return this.ordrCode;
    }

    public void setOrdrCode(String ordrCode) {
        this.ordrCode = ordrCode;
    }

    @Column(name = "`EXEC_START`", nullable = true)
    public LocalDateTime getExecStart() {
        return this.execStart;
    }

    public void setExecStart(LocalDateTime execStart) {
        this.execStart = execStart;
    }

    @Column(name = "`EXEC_END`", nullable = true)
    public LocalDateTime getExecEnd() {
        return this.execEnd;
    }

    public void setExecEnd(LocalDateTime execEnd) {
        this.execEnd = execEnd;
    }

    @Column(name = "`NC_NUMB`", nullable = true, length = 20)
    public String getNcNumb() {
        return this.ncNumb;
    }

    public void setNcNumb(String ncNumb) {
        this.ncNumb = ncNumb;
    }

    @Column(name = "`PL_END`", nullable = true)
    public LocalDateTime getPlEnd() {
        return this.plEnd;
    }

    public void setPlEnd(LocalDateTime plEnd) {
        this.plEnd = plEnd;
    }

    @Column(name = "`PROD_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getProdId() {
        return this.prodId;
    }

    public void setProdId(Integer prodId) {
        this.prodId = prodId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`PLANNER_ID`", referencedColumnName = "`ID`", insertable = false, updatable = false)
    public Employee getEmployee() {
        return this.employee;
    }

    public void setEmployee(Employee employee) {
        if(employee != null) {
            this.plannerId = employee.getId();
        }

        this.employee = employee;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`PROGR_ID`", referencedColumnName = "`ID`", insertable = false, updatable = false)
    public Program getProgram() {
        return this.program;
    }

    public void setProgram(Program program) {
        if(program != null) {
            this.progrId = program.getId();
        }

        this.program = program;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`PROD_ID`", referencedColumnName = "`PROD_ID`", insertable = false, updatable = false)
    public Product getProduct() {
        return this.product;
    }

    public void setProduct(Product product) {
        if(product != null) {
            this.prodId = product.getProdId();
        }

        this.product = product;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`PRIO_ID`", referencedColumnName = "`ID`", insertable = false, updatable = false)
    public Priorities getPriorities() {
        return this.priorities;
    }

    public void setPriorities(Priorities priorities) {
        if(priorities != null) {
            this.prioId = priorities.getId();
        }

        this.priorities = priorities;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "productionOrder")
    public List<JobOrder> getJobOrders() {
        return this.jobOrders;
    }

    public void setJobOrders(List<JobOrder> jobOrders) {
        this.jobOrders = jobOrders;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "productionOrder")
    public List<JobOrderMachines> getJobOrderMachineses() {
        return this.jobOrderMachineses;
    }

    public void setJobOrderMachineses(List<JobOrderMachines> jobOrderMachineses) {
        this.jobOrderMachineses = jobOrderMachineses;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "productionOrder")
    public List<ScheduleLines> getScheduleLineses() {
        return this.scheduleLineses;
    }

    public void setScheduleLineses(List<ScheduleLines> scheduleLineses) {
        this.scheduleLineses = scheduleLineses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductionOrder)) return false;
        final ProductionOrder productionOrder = (ProductionOrder) o;
        return Objects.equals(getId(), productionOrder.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

