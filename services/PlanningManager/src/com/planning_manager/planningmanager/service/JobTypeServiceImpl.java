/*Copyright (c) 2017-2018 vanenburgsoftware.com All Rights Reserved.
 This software is the confidential and proprietary information of vanenburgsoftware.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with vanenburgsoftware.com*/
package com.planning_manager.planningmanager.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.planning_manager.planningmanager.JobOrder;
import com.planning_manager.planningmanager.JobType;
import com.planning_manager.planningmanager.ScheduleTemplate;


/**
 * ServiceImpl object for domain model class JobType.
 *
 * @see JobType
 */
@Service("PlanningManager.JobTypeService")
public class JobTypeServiceImpl implements JobTypeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobTypeServiceImpl.class);

    @Autowired
	@Qualifier("PlanningManager.JobOrderService")
	private JobOrderService jobOrderService;

    @Autowired
	@Qualifier("PlanningManager.ScheduleTemplateService")
	private ScheduleTemplateService scheduleTemplateService;

    @Autowired
    @Qualifier("PlanningManager.JobTypeDao")
    private WMGenericDao<JobType, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<JobType, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "PlanningManagerTransactionManager")
    @Override
	public JobType create(JobType jobType) {
        LOGGER.debug("Creating a new JobType with information: {}", jobType);
        JobType jobTypeCreated = this.wmGenericDao.create(jobType);
        if(jobTypeCreated.getJobOrders() != null) {
            for(JobOrder jobOrder : jobTypeCreated.getJobOrders()) {
                jobOrder.setJobType(jobTypeCreated);
                LOGGER.debug("Creating a new child JobOrder with information: {}", jobOrder);
                jobOrderService.create(jobOrder);
            }
        }

        if(jobTypeCreated.getScheduleTemplatesForJobTp2Id() != null) {
            for(ScheduleTemplate scheduleTemplatesForJobTp2Id : jobTypeCreated.getScheduleTemplatesForJobTp2Id()) {
                scheduleTemplatesForJobTp2Id.setJobTypeByJobTp2Id(jobTypeCreated);
                LOGGER.debug("Creating a new child ScheduleTemplate with information: {}", scheduleTemplatesForJobTp2Id);
                scheduleTemplateService.create(scheduleTemplatesForJobTp2Id);
            }
        }

        if(jobTypeCreated.getScheduleTemplatesForJobTp1Id() != null) {
            for(ScheduleTemplate scheduleTemplatesForJobTp1Id : jobTypeCreated.getScheduleTemplatesForJobTp1Id()) {
                scheduleTemplatesForJobTp1Id.setJobTypeByJobTp1Id(jobTypeCreated);
                LOGGER.debug("Creating a new child ScheduleTemplate with information: {}", scheduleTemplatesForJobTp1Id);
                scheduleTemplateService.create(scheduleTemplatesForJobTp1Id);
            }
        }
        return jobTypeCreated;
    }

	@Transactional(readOnly = true, value = "PlanningManagerTransactionManager")
	@Override
	public JobType getById(Integer jobtypeId) throws EntityNotFoundException {
        LOGGER.debug("Finding JobType by id: {}", jobtypeId);
        JobType jobType = this.wmGenericDao.findById(jobtypeId);
        if (jobType == null){
            LOGGER.debug("No JobType found with id: {}", jobtypeId);
            throw new EntityNotFoundException(String.valueOf(jobtypeId));
        }
        return jobType;
    }

    @Transactional(readOnly = true, value = "PlanningManagerTransactionManager")
	@Override
	public JobType findById(Integer jobtypeId) {
        LOGGER.debug("Finding JobType by id: {}", jobtypeId);
        return this.wmGenericDao.findById(jobtypeId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "PlanningManagerTransactionManager")
	@Override
	public JobType update(JobType jobType) throws EntityNotFoundException {
        LOGGER.debug("Updating JobType with information: {}", jobType);
        this.wmGenericDao.update(jobType);

        Integer jobtypeId = jobType.getId();

        return this.wmGenericDao.findById(jobtypeId);
    }

    @Transactional(value = "PlanningManagerTransactionManager")
	@Override
	public JobType delete(Integer jobtypeId) throws EntityNotFoundException {
        LOGGER.debug("Deleting JobType with id: {}", jobtypeId);
        JobType deleted = this.wmGenericDao.findById(jobtypeId);
        if (deleted == null) {
            LOGGER.debug("No JobType found with id: {}", jobtypeId);
            throw new EntityNotFoundException(String.valueOf(jobtypeId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "PlanningManagerTransactionManager")
	@Override
	public Page<JobType> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all JobTypes");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "PlanningManagerTransactionManager")
    @Override
    public Page<JobType> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all JobTypes");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "PlanningManagerTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service PlanningManager for table JobType to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "PlanningManagerTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "PlanningManagerTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "PlanningManagerTransactionManager")
    @Override
    public Page<JobOrder> findAssociatedJobOrders(Integer id, Pageable pageable) {
        LOGGER.debug("Fetching all associated jobOrders");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("jobType.id = '" + id + "'");

        return jobOrderService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "PlanningManagerTransactionManager")
    @Override
    public Page<ScheduleTemplate> findAssociatedScheduleTemplatesForJobTp2Id(Integer id, Pageable pageable) {
        LOGGER.debug("Fetching all associated scheduleTemplatesForJobTp2Id");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("jobTypeByJobTp2Id.id = '" + id + "'");

        return scheduleTemplateService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "PlanningManagerTransactionManager")
    @Override
    public Page<ScheduleTemplate> findAssociatedScheduleTemplatesForJobTp1Id(Integer id, Pageable pageable) {
        LOGGER.debug("Fetching all associated scheduleTemplatesForJobTp1Id");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("jobTypeByJobTp1Id.id = '" + id + "'");

        return scheduleTemplateService.findAll(queryBuilder.toString(), pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service JobOrderService instance
	 */
	protected void setJobOrderService(JobOrderService service) {
        this.jobOrderService = service;
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service ScheduleTemplateService instance
	 */
	protected void setScheduleTemplateService(ScheduleTemplateService service) {
        this.scheduleTemplateService = service;
    }

}

