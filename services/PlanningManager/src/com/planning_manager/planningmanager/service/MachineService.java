/*Copyright (c) 2017-2018 vanenburgsoftware.com All Rights Reserved.
 This software is the confidential and proprietary information of vanenburgsoftware.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with vanenburgsoftware.com*/
package com.planning_manager.planningmanager.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.planning_manager.planningmanager.JobOrderMachines;
import com.planning_manager.planningmanager.Machine;

/**
 * Service object for domain model class {@link Machine}.
 */
public interface MachineService {

    /**
     * Creates a new Machine. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on Machine if any.
     *
     * @param machine Details of the Machine to be created; value cannot be null.
     * @return The newly created Machine.
     */
	Machine create(Machine machine);


	/**
	 * Returns Machine by given id if exists.
	 *
	 * @param machineId The id of the Machine to get; value cannot be null.
	 * @return Machine associated with the given machineId.
     * @throws EntityNotFoundException If no Machine is found.
	 */
	Machine getById(Integer machineId) throws EntityNotFoundException;

    /**
	 * Find and return the Machine by given id if exists, returns null otherwise.
	 *
	 * @param machineId The id of the Machine to get; value cannot be null.
	 * @return Machine associated with the given machineId.
	 */
	Machine findById(Integer machineId);


	/**
	 * Updates the details of an existing Machine. It replaces all fields of the existing Machine with the given machine.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on Machine if any.
     *
	 * @param machine The details of the Machine to be updated; value cannot be null.
	 * @return The updated Machine.
	 * @throws EntityNotFoundException if no Machine is found with given input.
	 */
	Machine update(Machine machine) throws EntityNotFoundException;

    /**
	 * Deletes an existing Machine with the given id.
	 *
	 * @param machineId The id of the Machine to be deleted; value cannot be null.
	 * @return The deleted Machine.
	 * @throws EntityNotFoundException if no Machine found with the given id.
	 */
	Machine delete(Integer machineId) throws EntityNotFoundException;

	/**
	 * Find all Machines matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Machines.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<Machine> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all Machines matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching Machines.
     *
     * @see Pageable
     * @see Page
	 */
    Page<Machine> findAll(String query, Pageable pageable);

    /**
	 * Exports all Machines matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the Machines in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the Machine.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);

    /*
     * Returns the associated jobOrderMachineses for given Machine id.
     *
     * @param id value of id; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated JobOrderMachines instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<JobOrderMachines> findAssociatedJobOrderMachineses(Integer id, Pageable pageable);

}

