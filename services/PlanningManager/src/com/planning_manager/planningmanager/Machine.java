/*Copyright (c) 2017-2018 vanenburgsoftware.com All Rights Reserved.
 This software is the confidential and proprietary information of vanenburgsoftware.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with vanenburgsoftware.com*/
package com.planning_manager.planningmanager;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Machine generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`Machine`")
public class Machine implements Serializable {

    private Integer id;
    private Integer machTypeId;
    private Integer depId;
    private String name;
    private MachineType machineType;
    private Department department;
    private List<JobOrderMachines> jobOrderMachineses;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ID`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`MACH_TYPE_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getMachTypeId() {
        return this.machTypeId;
    }

    public void setMachTypeId(Integer machTypeId) {
        this.machTypeId = machTypeId;
    }

    @Column(name = "`DEP_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getDepId() {
        return this.depId;
    }

    public void setDepId(Integer depId) {
        this.depId = depId;
    }

    @Column(name = "`NAME`", nullable = true, length = 100)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`MACH_TYPE_ID`", referencedColumnName = "`ID`", insertable = false, updatable = false)
    public MachineType getMachineType() {
        return this.machineType;
    }

    public void setMachineType(MachineType machineType) {
        if(machineType != null) {
            this.machTypeId = machineType.getId();
        }

        this.machineType = machineType;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`DEP_ID`", referencedColumnName = "`ID`", insertable = false, updatable = false)
    public Department getDepartment() {
        return this.department;
    }

    public void setDepartment(Department department) {
        if(department != null) {
            this.depId = department.getId();
        }

        this.department = department;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "machine")
    public List<JobOrderMachines> getJobOrderMachineses() {
        return this.jobOrderMachineses;
    }

    public void setJobOrderMachineses(List<JobOrderMachines> jobOrderMachineses) {
        this.jobOrderMachineses = jobOrderMachineses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Machine)) return false;
        final Machine machine = (Machine) o;
        return Objects.equals(getId(), machine.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

