/*Copyright (c) 2017-2018 vanenburgsoftware.com All Rights Reserved.
 This software is the confidential and proprietary information of vanenburgsoftware.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with vanenburgsoftware.com*/
package com.planning_manager.planningmanager;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Equipment generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`Equipment`")
public class Equipment implements Serializable {

    private Integer id;
    private String name;
    private Integer equipTpId;
    private EquipmentType equipmentType;
    private List<JobOrderEquipment> jobOrderEquipments;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ID`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`NAME`", nullable = true, length = 255)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "`EQUIP_TP_ID`", nullable = true, scale = 0, precision = 10)
    public Integer getEquipTpId() {
        return this.equipTpId;
    }

    public void setEquipTpId(Integer equipTpId) {
        this.equipTpId = equipTpId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`EQUIP_TP_ID`", referencedColumnName = "`ID`", insertable = false, updatable = false)
    public EquipmentType getEquipmentType() {
        return this.equipmentType;
    }

    public void setEquipmentType(EquipmentType equipmentType) {
        if(equipmentType != null) {
            this.equipTpId = equipmentType.getId();
        }

        this.equipmentType = equipmentType;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "equipment")
    public List<JobOrderEquipment> getJobOrderEquipments() {
        return this.jobOrderEquipments;
    }

    public void setJobOrderEquipments(List<JobOrderEquipment> jobOrderEquipments) {
        this.jobOrderEquipments = jobOrderEquipments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Equipment)) return false;
        final Equipment equipment = (Equipment) o;
        return Objects.equals(getId(), equipment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

